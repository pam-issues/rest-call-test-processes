package gtesb.ds1cgk65;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.1.16.redhat-1
 * 2019-01-17T14:39:09.205+03:00
 * Generated source version: 3.1.16.redhat-1
 * 
 */
@WebService(targetNamespace = "http://gtesb/DS1CGK65/", name = "DS1CGK65PortType")
@XmlSeeAlso({ObjectFactory.class, gtesb.ds1cgk65response.ObjectFactory.class, gtesb.ds1cgk65request.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface DS1CGK65PortType {

    @WebMethod(operationName = "DS1CGK65Operation", action = "http://gtesb/DS1CGK65/DS1CGK65Operation")
    @WebResult(name = "wsServiceResponse", targetNamespace = "http://gtesb/DS1CGK65Response", partName = "parameters")
    public gtesb.ds1cgk65response.WsServiceResponse ds1CGK65Operation(
        @WebParam(partName = "parameters", name = "wsServiceRequest", targetNamespace = "http://gtesb/DS1CGK65Request")
        gtesb.ds1cgk65request.WsServiceRequest parameters
    );
}
