package gtesb.ds1cgk65;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.16.redhat-1
 * 2019-01-17T14:39:09.245+03:00
 * Generated source version: 3.1.16.redhat-1
 * 
 */
@WebServiceClient(name = "DS1CGK65Service", 
                  wsdlLocation = "file:/home/donato/Documents/VID/RH/Customers/Garanti/wsdl/DS1CK650.wsdl",
                  targetNamespace = "http://gtesb/DS1CGK65/") 
public class DS1CGK65Service extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://gtesb/DS1CGK65/", "DS1CGK65Service");
    public final static QName DS1CGK65SOAPPort = new QName("http://gtesb/DS1CGK65/", "DS1CGK65SOAPPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/home/donato/Documents/VID/RH/Customers/Garanti/wsdl/DS1CK650.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(DS1CGK65Service.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/home/donato/Documents/VID/RH/Customers/Garanti/wsdl/DS1CK650.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public DS1CGK65Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public DS1CGK65Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public DS1CGK65Service() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public DS1CGK65Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public DS1CGK65Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public DS1CGK65Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns DS1CGK65PortType
     */
    @WebEndpoint(name = "DS1CGK65SOAPPort")
    public DS1CGK65PortType getDS1CGK65SOAPPort() {
        return super.getPort(DS1CGK65SOAPPort, DS1CGK65PortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns DS1CGK65PortType
     */
    @WebEndpoint(name = "DS1CGK65SOAPPort")
    public DS1CGK65PortType getDS1CGK65SOAPPort(WebServiceFeature... features) {
        return super.getPort(DS1CGK65SOAPPort, DS1CGK65PortType.class, features);
    }

}
