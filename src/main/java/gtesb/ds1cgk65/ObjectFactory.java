
package gtesb.ds1cgk65;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gtesb.ds1cgk65 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DS1CGK65Operation_QNAME = new QName("http://gtesb/DS1CGK65/", "DS1CGK65Operation");
    private final static QName _DS1CGK65OperationResponse_QNAME = new QName("http://gtesb/DS1CGK65/", "DS1CGK65OperationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gtesb.ds1cgk65
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DS1CGK65OperationType }
     * 
     */
    public DS1CGK65OperationType createDS1CGK65OperationType() {
        return new DS1CGK65OperationType();
    }

    /**
     * Create an instance of {@link DS1CGK65OperationResponseType }
     * 
     */
    public DS1CGK65OperationResponseType createDS1CGK65OperationResponseType() {
        return new DS1CGK65OperationResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DS1CGK65OperationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gtesb/DS1CGK65/", name = "DS1CGK65Operation")
    public JAXBElement<DS1CGK65OperationType> createDS1CGK65Operation(DS1CGK65OperationType value) {
        return new JAXBElement<DS1CGK65OperationType>(_DS1CGK65Operation_QNAME, DS1CGK65OperationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DS1CGK65OperationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gtesb/DS1CGK65/", name = "DS1CGK65OperationResponse")
    public JAXBElement<DS1CGK65OperationResponseType> createDS1CGK65OperationResponse(DS1CGK65OperationResponseType value) {
        return new JAXBElement<DS1CGK65OperationResponseType>(_DS1CGK65OperationResponse_QNAME, DS1CGK65OperationResponseType.class, null, value);
    }

}
