
package gtesb.ds1cgk65request;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ck650Import complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Ck650Import"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="iDialectCd"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProcessMainCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProcessSubCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProcessCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProcessId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProductCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="11"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iProductId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="iTxnId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="26"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ck650Import", propOrder = {
    "iDialectCd",
    "iProcessMainCode",
    "iProcessSubCode",
    "iProcessCode",
    "iProcessId",
    "iProductCode",
    "iProductId",
    "iTxnId"
})
public class Ck650Import
    implements Serializable
{

    @XmlElement(required = true)
    protected String iDialectCd;
    @XmlElement(required = true)
    protected String iProcessMainCode;
    @XmlElement(required = true)
    protected String iProcessSubCode;
    @XmlElement(required = true)
    protected String iProcessCode;
    @XmlElement(required = true)
    protected String iProcessId;
    @XmlElement(required = true)
    protected String iProductCode;
    @XmlElement(required = true)
    protected String iProductId;
    @XmlElement(required = true)
    protected String iTxnId;

    /**
     * Gets the value of the iDialectCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDialectCd() {
        return iDialectCd;
    }

    /**
     * Sets the value of the iDialectCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDialectCd(String value) {
        this.iDialectCd = value;
    }

    /**
     * Gets the value of the iProcessMainCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProcessMainCode() {
        return iProcessMainCode;
    }

    /**
     * Sets the value of the iProcessMainCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProcessMainCode(String value) {
        this.iProcessMainCode = value;
    }

    /**
     * Gets the value of the iProcessSubCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProcessSubCode() {
        return iProcessSubCode;
    }

    /**
     * Sets the value of the iProcessSubCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProcessSubCode(String value) {
        this.iProcessSubCode = value;
    }

    /**
     * Gets the value of the iProcessCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProcessCode() {
        return iProcessCode;
    }

    /**
     * Sets the value of the iProcessCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProcessCode(String value) {
        this.iProcessCode = value;
    }

    /**
     * Gets the value of the iProcessId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProcessId() {
        return iProcessId;
    }

    /**
     * Sets the value of the iProcessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProcessId(String value) {
        this.iProcessId = value;
    }

    /**
     * Gets the value of the iProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProductCode() {
        return iProductCode;
    }

    /**
     * Sets the value of the iProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProductCode(String value) {
        this.iProductCode = value;
    }

    /**
     * Gets the value of the iProductId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProductId() {
        return iProductId;
    }

    /**
     * Sets the value of the iProductId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProductId(String value) {
        this.iProductId = value;
    }

    /**
     * Gets the value of the iTxnId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITxnId() {
        return iTxnId;
    }

    /**
     * Sets the value of the iTxnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITxnId(String value) {
        this.iTxnId = value;
    }

}
