
package gtesb.ds1cgk65request;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsServiceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsServiceRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hdrCompanyId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hdrPlatformType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hdrUserId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hdrPassword"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hdrToken"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="64"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hdrDialectCd"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="logDisplayYN"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ck650Import" type="{http://gtesb/DS1CGK65Request}Ck650Import"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsServiceRequest", propOrder = {
    "hdrCompanyId",
    "hdrPlatformType",
    "hdrUserId",
    "hdrPassword",
    "hdrToken",
    "hdrDialectCd",
    "logDisplayYN",
    "ck650Import"
})
public class WsServiceRequest
    implements Serializable
{

    @XmlElement(required = true)
    protected String hdrCompanyId;
    @XmlElement(required = true)
    protected String hdrPlatformType;
    @XmlElement(required = true)
    protected String hdrUserId;
    @XmlElement(required = true)
    protected String hdrPassword;
    @XmlElement(required = true)
    protected String hdrToken;
    @XmlElement(required = true)
    protected String hdrDialectCd;
    @XmlElement(required = true)
    protected String logDisplayYN;
    @XmlElement(required = true)
    protected Ck650Import ck650Import;

    /**
     * Gets the value of the hdrCompanyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrCompanyId() {
        return hdrCompanyId;
    }

    /**
     * Sets the value of the hdrCompanyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrCompanyId(String value) {
        this.hdrCompanyId = value;
    }

    /**
     * Gets the value of the hdrPlatformType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrPlatformType() {
        return hdrPlatformType;
    }

    /**
     * Sets the value of the hdrPlatformType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrPlatformType(String value) {
        this.hdrPlatformType = value;
    }

    /**
     * Gets the value of the hdrUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrUserId() {
        return hdrUserId;
    }

    /**
     * Sets the value of the hdrUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrUserId(String value) {
        this.hdrUserId = value;
    }

    /**
     * Gets the value of the hdrPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrPassword() {
        return hdrPassword;
    }

    /**
     * Sets the value of the hdrPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrPassword(String value) {
        this.hdrPassword = value;
    }

    /**
     * Gets the value of the hdrToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrToken() {
        return hdrToken;
    }

    /**
     * Sets the value of the hdrToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrToken(String value) {
        this.hdrToken = value;
    }

    /**
     * Gets the value of the hdrDialectCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdrDialectCd() {
        return hdrDialectCd;
    }

    /**
     * Sets the value of the hdrDialectCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdrDialectCd(String value) {
        this.hdrDialectCd = value;
    }

    /**
     * Gets the value of the logDisplayYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogDisplayYN() {
        return logDisplayYN;
    }

    /**
     * Sets the value of the logDisplayYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogDisplayYN(String value) {
        this.logDisplayYN = value;
    }

    /**
     * Gets the value of the ck650Import property.
     * 
     * @return
     *     possible object is
     *     {@link Ck650Import }
     *     
     */
    public Ck650Import getCk650Import() {
        return ck650Import;
    }

    /**
     * Sets the value of the ck650Import property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ck650Import }
     *     
     */
    public void setCk650Import(Ck650Import value) {
        this.ck650Import = value;
    }

}
