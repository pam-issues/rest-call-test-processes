
package gtesb.ds1cgk65response;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ck650Export complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Ck650Export"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eCurrentPrcsLeve"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eProcessEscFlo"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eProcessMainCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eProcessSubCode"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eEscPcsType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eVrfUnitNum"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *               &lt;totalDigits value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eAprvUnitNum"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *               &lt;totalDigits value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="eEscUnitNum"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *               &lt;totalDigits value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ck650Export", propOrder = {
    "eCurrentPrcsLeve",
    "eProcessEscFlo",
    "eProcessMainCode",
    "eProcessSubCode",
    "eEscPcsType",
    "eVrfUnitNum",
    "eAprvUnitNum",
    "eEscUnitNum"
})
public class Ck650Export
    implements Serializable
{

    @XmlElement(required = true)
    protected String eCurrentPrcsLeve;
    @XmlElement(required = true)
    protected String eProcessEscFlo;
    @XmlElement(required = true)
    protected String eProcessMainCode;
    @XmlElement(required = true)
    protected String eProcessSubCode;
    @XmlElement(required = true)
    protected String eEscPcsType;
    protected long eVrfUnitNum;
    protected long eAprvUnitNum;
    protected long eEscUnitNum;

    /**
     * Gets the value of the eCurrentPrcsLeve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECurrentPrcsLeve() {
        return eCurrentPrcsLeve;
    }

    /**
     * Sets the value of the eCurrentPrcsLeve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECurrentPrcsLeve(String value) {
        this.eCurrentPrcsLeve = value;
    }

    /**
     * Gets the value of the eProcessEscFlo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEProcessEscFlo() {
        return eProcessEscFlo;
    }

    /**
     * Sets the value of the eProcessEscFlo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEProcessEscFlo(String value) {
        this.eProcessEscFlo = value;
    }

    /**
     * Gets the value of the eProcessMainCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEProcessMainCode() {
        return eProcessMainCode;
    }

    /**
     * Sets the value of the eProcessMainCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEProcessMainCode(String value) {
        this.eProcessMainCode = value;
    }

    /**
     * Gets the value of the eProcessSubCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEProcessSubCode() {
        return eProcessSubCode;
    }

    /**
     * Sets the value of the eProcessSubCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEProcessSubCode(String value) {
        this.eProcessSubCode = value;
    }

    /**
     * Gets the value of the eEscPcsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEEscPcsType() {
        return eEscPcsType;
    }

    /**
     * Sets the value of the eEscPcsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEEscPcsType(String value) {
        this.eEscPcsType = value;
    }

    /**
     * Gets the value of the eVrfUnitNum property.
     * 
     */
    public long getEVrfUnitNum() {
        return eVrfUnitNum;
    }

    /**
     * Sets the value of the eVrfUnitNum property.
     * 
     */
    public void setEVrfUnitNum(long value) {
        this.eVrfUnitNum = value;
    }

    /**
     * Gets the value of the eAprvUnitNum property.
     * 
     */
    public long getEAprvUnitNum() {
        return eAprvUnitNum;
    }

    /**
     * Sets the value of the eAprvUnitNum property.
     * 
     */
    public void setEAprvUnitNum(long value) {
        this.eAprvUnitNum = value;
    }

    /**
     * Gets the value of the eEscUnitNum property.
     * 
     */
    public long getEEscUnitNum() {
        return eEscUnitNum;
    }

    /**
     * Sets the value of the eEscUnitNum property.
     * 
     */
    public void setEEscUnitNum(long value) {
        this.eEscUnitNum = value;
    }

}
