
package gtesb.ds1cgk65response;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExpError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExpError"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="returnCodea"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;totalDigits value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reasonCodea"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;totalDigits value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="messageTxt"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="512"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpError", propOrder = {
    "returnCodea",
    "reasonCodea",
    "messageTxt"
})
public class ExpError
    implements Serializable
{

    protected int returnCodea;
    protected int reasonCodea;
    @XmlElement(required = true)
    protected String messageTxt;

    /**
     * Gets the value of the returnCodea property.
     * 
     */
    public int getReturnCodea() {
        return returnCodea;
    }

    /**
     * Sets the value of the returnCodea property.
     * 
     */
    public void setReturnCodea(int value) {
        this.returnCodea = value;
    }

    /**
     * Gets the value of the reasonCodea property.
     * 
     */
    public int getReasonCodea() {
        return reasonCodea;
    }

    /**
     * Sets the value of the reasonCodea property.
     * 
     */
    public void setReasonCodea(int value) {
        this.reasonCodea = value;
    }

    /**
     * Gets the value of the messageTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageTxt() {
        return messageTxt;
    }

    /**
     * Sets the value of the messageTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageTxt(String value) {
        this.messageTxt = value;
    }

}
