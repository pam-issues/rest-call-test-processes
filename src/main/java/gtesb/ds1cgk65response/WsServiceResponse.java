
package gtesb.ds1cgk65response;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsServiceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsServiceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ck650Export" type="{http://gtesb/DS1CGK65Response}Ck650Export"/&gt;
 *         &lt;element name="expError" type="{http://gtesb/DS1CGK65Response}ExpError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsServiceResponse", propOrder = {
    "ck650Export",
    "expError"
})
public class WsServiceResponse
    implements Serializable
{

    @XmlElement(required = true)
    protected Ck650Export ck650Export;
    @XmlElement(required = true)
    protected ExpError expError;

    /**
     * Gets the value of the ck650Export property.
     * 
     * @return
     *     possible object is
     *     {@link Ck650Export }
     *     
     */
    public Ck650Export getCk650Export() {
        return ck650Export;
    }

    /**
     * Sets the value of the ck650Export property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ck650Export }
     *     
     */
    public void setCk650Export(Ck650Export value) {
        this.ck650Export = value;
    }

    /**
     * Gets the value of the expError property.
     * 
     * @return
     *     possible object is
     *     {@link ExpError }
     *     
     */
    public ExpError getExpError() {
        return expError;
    }

    /**
     * Sets the value of the expError property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpError }
     *     
     */
    public void setExpError(ExpError value) {
        this.expError = value;
    }

}
